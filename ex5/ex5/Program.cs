﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex5
{
    class Program
    {
        static void Main(string[] args)
        {
            //True/False quiz thing - complicated
            Console.WriteLine("Welcome!");

            var answer = false;
            var score = 0;

            //Question 1
            Console.WriteLine("Is the sky blue?");
            var userAnswer = Console.ReadLine();

            if (userAnswer == "true" || userAnswer == "false")
            {
                answer = bool.Parse(userAnswer);
                if (answer == true)
                {
                    score++;
                    //Question 2
                    Console.WriteLine("Is Ashton a man?");
                    userAnswer = Console.ReadLine();

                    if (userAnswer == "true" || userAnswer == "false")
                    {
                        answer = bool.Parse(userAnswer);
                        if (answer == true)
                        {
                            score++;
                            //Question 3
                            Console.WriteLine("Are there 26 letters in the Alphabet?");
                            userAnswer = Console.ReadLine();

                            if (userAnswer == "true" || userAnswer == "false")
                            {
                                answer = bool.Parse(userAnswer);
                                if (answer == true)
                                {
                                    score++;
                                }
                                else
                                {
                                    Console.WriteLine($"You got {score} points");
                                    return;
                                }

                            }
                        }
                        else
                        {
                            Console.WriteLine($"You got {score} points");
                            return;
                        }

                    }

                }
                else
                {
                    Console.WriteLine($"You got {score} points");
                    return;
                }
            }
            else
            {
                Console.WriteLine($"You got {score} points");
            }
            



        }
    }
}
