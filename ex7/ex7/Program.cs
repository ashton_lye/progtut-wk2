﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex7
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 5;

            Console.WriteLine("Please enter a number:");
            var i = int.Parse(Console.ReadLine());

            do
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");

                i++;
            } while (i < counter);
        }
    }
}
