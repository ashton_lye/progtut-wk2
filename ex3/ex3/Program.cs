﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Switch statement with user input - mile to km converter
            Console.WriteLine("What would you like to do?");
            Console.WriteLine("1. Convert Miles to Kilometers");
            Console.WriteLine("2. Convert Kilometers to Miles");
            var menuSelection = Console.ReadLine();

            switch(menuSelection)
            {
                case "1":
                    Console.WriteLine("You chose to convert miles to kilometers");
                    Console.WriteLine("How many miles to convert?");
                    var miles = double.Parse(Console.ReadLine());
                    Console.WriteLine($"{miles} miles is equal to {miles * 0.621371} kilometers");
                    break;
                case "2":
                    Console.WriteLine("You chose to convert kilometers to miles");
                    Console.WriteLine("How many kilometers to convert?");
                    var kilometers = double.Parse(Console.ReadLine());
                    Console.WriteLine($"{kilometers} kilometers is equal to {kilometers * 1.609344} miles");
                    break;
                default:
                    Console.WriteLine("Please enter a valid number");
                    break;
            }
        }
    }
}
