﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            var number1 = 10;
            var number2 = 4;

            if (number1 > number2)
            {
                Console.WriteLine("Number1 is bigger than number2");
            }
            else
            {
                Console.WriteLine("Number1 is smaller or equal to number2");
            }
        }
    }
}
