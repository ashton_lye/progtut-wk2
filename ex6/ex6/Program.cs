﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex6
{
    class Program
    {
        static void Main(string[] args)
        {
            //while loop
            var counter = 5;
            var i = 0;

            while (i < counter)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");

                i++;
            }
        }
    }
}
